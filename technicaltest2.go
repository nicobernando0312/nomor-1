package main

import (
    "log"
    "database/sql"
    "net/http"
    "github.com/gorilla/mux"
    "encoding/json"
    _ "github.com/go-sql-driver/mysql"
)

type Employee struct{
        Id        string `form:"id" json:"id"`
        Full_Name string `form:"full_name" json:"full_name"`
        Address  string `form:"address" json:"address"`
        DOB string `form:"dob" json:"dob"`
        Role_Id string `form:"role_id" json:"role_id"`
        Role_Name  string `form:"role_name" json:"role_name"`
        Salary string `form:"salary" json:"salary"`
}

func connect() *sql.DB{

    db, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/employee")
    if err != nil {
        panic(err.Error())
    }

    return db

}

func getEmployees(w http.ResponseWriter, r *http.Request){
    var employees Employee
    var arr_emp []Employee

    db := connect()

    selectDB, err := db.Query("select e.id, e.full_name, e.address, e.dob, e.role_id, r.role_name, s.salary from employee as e join salary as s on s.employee_id = e.id join role as r on r.role_id = e.role_id")
   
    if err != nil {
		log.Fatal(err)
	}
    
    for selectDB.Next() {
		if err := selectDB.Scan(&employees.Id, &employees.Full_Name, &employees.Address, &employees.DOB, &employees.Role_Id, &employees.Role_Name, &employees.Salary); err != nil {
			log.Fatal(err.Error())

		} else {
			arr_emp = append(arr_emp, employees)
		}
    }
    
    w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(arr_emp)
    defer db.Close()
}

func getEmployeesId(w http.ResponseWriter, r *http.Request){
    employeesid := mux.Vars(r)["id"]
    var employees Employee
    var arr_emp []Employee

    db := connect()

    selectDB, err := db.Query("select e.id, e.full_name, e.address, e.dob, e.role_id, r.role_name, s.salary from employee as e join salary as s on s.employee_id = e.id join role as r on r.role_id = e.role_id where e.id="+employeesid)
   
    if err != nil {
		log.Fatal(err)
	}
    
    for selectDB.Next() {
		if err := selectDB.Scan(&employees.Id, &employees.Full_Name, &employees.Address, &employees.DOB, &employees.Role_Id, &employees.Role_Name, &employees.Salary); err != nil {
			log.Fatal(err.Error())

		} else {
			arr_emp = append(arr_emp, employees)
		}
    }
    
    w.Header().Set("Content-Type", "application/json")
    json.NewEncoder(w).Encode(arr_emp)
    defer db.Close()
}

func deleteEmp(w http.ResponseWriter, r *http.Request){
    employeesid := mux.Vars(r)["id"]
    var employees Employee
    var arr_emp []Employee

    db := connect()

    selectDB, err := db.Query("delete from employee where id="+employeesid)
   
    if err != nil {
		log.Fatal(err)
	}
    
    defer db.Close()
}

func main(){
    router := mux.NewRouter().StrictSlash(true)
    router.HandleFunc("/employee", getEmployees).Methods("GET")
    router.HandleFunc("/employee/{id}", getEmployeesId).Methods("GET")
    router.HandleFunc("/employee/{id}", deleteEmp).Methods("POST")
    log.Fatal(http.ListenAndServe(":8080", router))
}