package main

import (
    "fmt"
    "io/ioutil"
    "log"
    "net/http"
    "github.com/gorilla/mux"
    "encoding/json"
)

type Breed struct {
    Breed []string `json:"message"`
}


func getEvent(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")
    response, err := http.Get("https://dog.ceo/api/breeds/list")
    if err != nil {
	//Kalau error
        fmt.Printf("The HTTP request failed with error %s\n", err)
    } else {
        data, _ := ioutil.ReadAll(response.Body)
        fmt.Println(string(data))
        w.Write(data)
        var dog Breed
        json.Unmarshal(data, &dog)
        //fmt.Println(dog);
        // fmt.Println("Dog 1 :", dog[0].breed)
        // fmt.Println("Dog 2 :", dog[1].breed)
        dogbreed := Breed{
            Breed : dog
        }

        jsonData := json.Marshal(dogbreed)
        fmt.Println(string(jsonData))

    }
    
}

func getEventBreed(w http.ResponseWriter, r *http.Request){
    eventBreed := mux.Vars(r)["breed"]
    w.Header().Set("Content-Type", "application/json")
    response, err := http.Get("https://dog.ceo/api/breed/"+eventBreed+"/list")
    if err != nil {
	//Kalau error
        fmt.Printf("The HTTP request failed with error %s\n", err)
    } else {
        data, _ := ioutil.ReadAll(response.Body)
        // fmt.Println(string(data))
        w.Write([]byte(string(data)))
    }
}

func main() {
    router := mux.NewRouter().StrictSlash(true)
    router.HandleFunc("/dogs", getEvent).Methods("GET")
    router.HandleFunc("/dogs/{breed}", getEventBreed).Methods("GET")
    log.Fatal(http.ListenAndServe(":8080", router))
}